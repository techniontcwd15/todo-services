import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class StateService {
  public itemsSubject = new Subject();
  currentItem:number = 0;
  items = ['first item', '2nd item', '3rd item', '4th item'];

  setItemValue(itemValue) {
    this.items[this.currentItem] = itemValue;
    this.itemsSubject.next(this.items);
  }

  setItem(newItem:number) {
    this.currentItem = newItem;
  }
  constructor() { }

}
