import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {
  items = this.state.items;

  constructor(private state:StateService) {
    state.itemsSubject.subscribe((newItems:string[]) => {
      this.items = newItems;
    });
  }

  ngOnInit() {
  }

}
