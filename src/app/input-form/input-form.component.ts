import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  constructor(private state:StateService) {
  }

  updateItemValue(inputValue) {
    this.state.setItemValue(inputValue);
  }

  updateItemNumber(itemNumber:string) {
    this.state.setItem(parseInt(itemNumber));
  }

  ngOnInit() {
  }

}
