import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InputFormComponent } from './input-form/input-form.component';
import { ItemComponent } from './item/item.component';
import { ItemsListComponent } from './items-list/items-list.component';
import { StateService } from './state.service';

@NgModule({
  declarations: [
    AppComponent,
    InputFormComponent,
    ItemComponent,
    ItemsListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [StateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
